﻿using Microsoft.AspNetCore.SignalR.Client;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace ClientApp
{
    class Program
    {
        static HubConnection connection;
        const int REPEAT_INTERVAL = 30 * 1000; // 30 seconds
        static string deviceId = "EMPTY";
        static Timer loop;

        static async Task Main(string[] args)
        {
            if (args.Length == 0)
            {
                throw new ArgumentException("1st argument must be Device Id/Name");
            }

            deviceId = args[0].ToString();

            connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:5001/serverHub")
                .Build();

            connection.Closed += async (error) =>
            {
                loop.Dispose();

                await Task.Delay(new Random().Next(0, 5) * 1000);

                await Connect();
            };

            connection.On<string>("ReceiveMessage", (message) =>
            {
                LogIn(message);
            });

            connection.On<string>("ManagerDisconnected", (managerUid) =>
            {
                LogInfo($"Manager '{managerUid}' has disconnected ...");
            });

            connection.On<string>("ManagerConnected", (managerUid) =>
            {
                LogInfo($"Manager '{managerUid}' has connected ...");
            });

            await Connect();

            Console.ReadKey();
        }

        private static async Task Connect()
        {
            try
            {
                LogInfo("Connecting ...");

                await connection.StartAsync();

                await Register();

                loop = new Timer(x => SendHeartBeat(), null, 0, REPEAT_INTERVAL);

                LogInfo("Connected!");
            }
            catch (Exception ex)
            {
                LogError(ex);

                await Task.Delay(new Random().Next(0, 5) * 1000);

                await Connect();
            }
        }

        static async Task Register()
        {
            await connection.SendAsync("RegisterManager", deviceId);
        }

        static void SendHeartBeat()
        {
            connection.InvokeAsync("HeartBeat", deviceId);
        }


        static void LogIn(string message)
        {
            Console.WriteLine($"IN: {message}");
        }

        static void LogOut(string message)
        {
            Console.WriteLine($"OUT: {message}");
        }

        static void LogInfo(string message)
        {
            Console.WriteLine($"INFO: {message}");
        }

        static void LogError(Exception ex)
        {
            Console.WriteLine($"ERROR: {ex.Message}");
        }
    }
}
