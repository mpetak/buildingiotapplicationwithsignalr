﻿using ServerWebApp.Shared.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ServerWebApp.Shared.Interfaces
{
    public interface IWebAppHub
    {
        Task ReceiveMessage(string timestampWithId);

        Task ManagerConnected(string managerName);

        Task ManagerDisconnected(string managerName);

        Task ManagerList(List<Manager> managers);
    }
}
