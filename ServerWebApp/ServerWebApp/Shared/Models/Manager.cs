﻿namespace ServerWebApp.Shared.Models
{
    public class Manager
    {
        public string ConnectionId { get; set; }

        public string Name { get; set; }

        public bool Connected { get; set; }
    }
}
