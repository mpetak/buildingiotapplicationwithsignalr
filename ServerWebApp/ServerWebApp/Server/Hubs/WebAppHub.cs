﻿using Microsoft.AspNetCore.SignalR;
using ServerWebApp.Shared;
using ServerWebApp.Shared.Interfaces;
using ServerWebApp.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServerWebApp.Server.Hubs
{
    public class WebAppHub : Hub<IWebAppHub>
    {
        
        private static List<Manager> managers = new List<Manager>();

        public async Task HeartBeat(string managerName)
        {
            var foo = DateTime.Now;
            var unixTime = ((DateTimeOffset)foo).ToUnixTimeSeconds();

            await Clients.Group(AppConstants.MANAGER_GROUP).ReceiveMessage($"{unixTime}_{AppConstants.APP_UID}");
        }

        public async Task RegisterManager(string name)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, AppConstants.MANAGER_GROUP);

            var manager = managers.Where(x => x.Name == name).FirstOrDefault();
            if (manager == null)
            {
                manager = new Manager { ConnectionId = Context.ConnectionId, Name = name, Connected = true };
                managers.Add(manager);
            }
            else
            {
                manager.ConnectionId = Context.ConnectionId;
                manager.Connected = true;
            }

            await SendManagerList();

            await Clients.Group(AppConstants.MANAGER_GROUP).ManagerConnected(manager.Name);
        }

        public async Task RegisterToMasterGroup()
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, AppConstants.MASTER_GROUP);
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var manager = managers.Where(x => x.ConnectionId == Context.ConnectionId).FirstOrDefault();

            if (manager != null)
            {
                manager.ConnectionId = string.Empty;
                manager.Connected = false;
                
                await SendManagerList();
                await Clients.Group(AppConstants.MANAGER_GROUP).ManagerDisconnected(manager.Name);
            }
        }

        public async Task SendManagerList()
        {
            await Clients.Group(AppConstants.MASTER_GROUP).ManagerList(managers);
        }
    }
}